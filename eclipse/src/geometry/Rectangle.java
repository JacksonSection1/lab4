// Jackson Kwan 1938023
package geometry;

public class Rectangle implements Shape{
	private double width;
	private double length;
	
	public Rectangle(double width, double length) {
		this.width = width;
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public double getLength() {
		return length;
	}
	
	public double getArea() {
		return width*length;
	}
	
	public double getPerimeter() {
		return width*2+length*2;
	}
}

// Jackson Kwan 1938023
package geometry;

public class Circle implements Shape {
	private double radius;
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public double getArea() {
		return Math.pow(radius, 2) * Math.PI;
	}
	
	public double getPerimeter() {
		return 2*radius*Math.PI;
	}
}

// Jackson Kwan 1938023
package geometry;

public interface Shape {
	double getArea();
	double getPerimeter();
}

//Jackson Kwan 1938023
package inheritance;

public class Book {
	protected String title;
	private String author;
	
	public String getTitle() {
		return title;
	}
	public String getAuthor() {
		return author;
	}
	
	public Book(String title, String author) {
		this.title = title;
		this.author = author;
	}
	
	public String toString() {
		return ("Title: " + title + " Author: " + author + " ");
	}
}
